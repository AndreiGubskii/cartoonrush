﻿using UnityEngine;
using UnityEditor;

public class CreatePlayerVehicle : EditorWindow {

    int normalTextSize;
    string ObjectName = "";
    GameObject vehicleModel, explosion;
    Material explosionMat;
    AudioClip crashSound, swipeSound, coinSound;

    [MenuItem("Tools/Create Player Vehicle")]
    public static void ShowWindow()
    {
        CreatePlayerVehicle window = EditorWindow.GetWindow<CreatePlayerVehicle>(true, "Player Vehicle");
        window.maxSize = new Vector2(500f, 300f);
        window.minSize = window.maxSize;
    }

    void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.Space(20);

        //Title Name
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        normalTextSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = 25;
        GUILayout.Label("Create Player Vehicle");
        GUI.skin.label.fontSize = normalTextSize;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        //Vehicle Name Text Field
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Name", GUILayout.Width(100));
        ObjectName = EditorGUILayout.TextField(ObjectName);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        //reference to model
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Model", GUILayout.Width(100));
        vehicleModel = (GameObject)EditorGUILayout.ObjectField(vehicleModel, typeof(GameObject), false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //reference to explosion
        GUILayout.BeginHorizontal();
        GUILayout.Label("Explosion", GUILayout.Width(100));
        explosion = (GameObject)EditorGUILayout.ObjectField(explosion, typeof(GameObject), false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //reference to explosion material
        GUILayout.BeginHorizontal();
        GUILayout.Label("Explosion Mat", GUILayout.Width(100));
        explosionMat = (Material)EditorGUILayout.ObjectField(explosionMat, typeof(Material), false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //reference to swipe sound
        GUILayout.BeginHorizontal();
        GUILayout.Label("Swipe Sound", GUILayout.Width(100));
        swipeSound = (AudioClip)EditorGUILayout.ObjectField(swipeSound, typeof(AudioClip), false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //reference to crash sound
        GUILayout.BeginHorizontal();
        GUILayout.Label("Crash Sound", GUILayout.Width(100));
        crashSound = (AudioClip)EditorGUILayout.ObjectField(crashSound, typeof(AudioClip), false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //reference to coin sound
        GUILayout.BeginHorizontal();
        GUILayout.Label("Coin Sound", GUILayout.Width(100));
        coinSound = (AudioClip)EditorGUILayout.ObjectField(coinSound, typeof(AudioClip), false);
        GUILayout.EndHorizontal();

        //Create Btn and Warnings
        GUILayout.Space(10);
        if (ObjectName == "" || vehicleModel == null || swipeSound == null || 
            crashSound == null || coinSound == null || explosion == null || explosionMat == null)
        {
            EditorGUILayout.HelpBox("You are Missing something", MessageType.Warning);
        }
        else
        {
            if (GUILayout.Button("Create" , GUILayout.Height(50)))
            {
                CreateVehicle();
            }
        }

        GUILayout.EndVertical();
    }

    void CreateVehicle()
    {
        var root              = new GameObject(ObjectName);
        root.tag              = "Player";
        var rootBody          = root.AddComponent<Rigidbody>();
        rootBody.useGravity   = false;
        var boxCollider       = root.AddComponent<BoxCollider>();
        boxCollider.size      = new Vector3(2.6f, 1.8f, 5.2f);
        boxCollider.center    = new Vector3(0, 1.4f, 0);
        boxCollider.isTrigger = true;

        var controllerScript               = root.AddComponent<PlayerController>();
        controllerScript.carSpeed          = 8;
        controllerScript.turnSpeed         = 0.2f;
        controllerScript.limits            = new float[] { -10, 10 };
        controllerScript.lanePos           = new float[] { 10, 5, 0, -5, -10 };
        controllerScript.levelUpScore      = 100;
        controllerScript.isDoubleSpeed     = 1;
        controllerScript.brakeSpeed        = 1;
        controllerScript.crashSound        = crashSound;
        controllerScript.coinPickUp        = coinSound;
        controllerScript.swipeSound        = swipeSound;
        controllerScript.explosionMat      = explosionMat;
        controllerScript.currentLane       = PlayerLane.three;

        var meshHolder                     = new GameObject("MeshHolder");
        var meshAnim                       = meshHolder.AddComponent<Animator>();
        meshAnim.runtimeAnimatorController = Resources.Load("CarModel") as RuntimeAnimatorController;
        meshHolder.transform.parent        = root.transform;
        GameObject meshModel               = Instantiate(vehicleModel);
        meshModel.transform.parent         = meshHolder.transform;
        meshModel.transform.position       = Vector3.up;
        GameObject explosionHolder         = Instantiate(explosion);
        explosionHolder.name               = "ExplosionHolder";
        explosionHolder.transform.parent   = root.transform;
        explosionHolder.transform.position = Vector3.zero;
        controllerScript.explosionHolder   = explosionHolder;
        controllerScript.carAnim           = meshHolder.GetComponent<Animator>();
        explosion.SetActive(false);

        Debug.Log("VehicleCreated");
    }

}
