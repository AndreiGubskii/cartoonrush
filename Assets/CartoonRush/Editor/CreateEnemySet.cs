﻿using UnityEngine;
using UnityEditor;

public class CreateEnemySet : EditorWindow
{
    int normalTextSize;
    string ObjectName = "";
    EnemyCarController vehicleModel1, vehicleModel2;
    int vehicle1Lane, vehicle2Lane;
    int vehicle1Dir, vehicle2Dir;
    string[] option = new string[] { "Left", "Right" , "Straight"};

    [MenuItem("Tools/Create Enemy Set")]
    public static void ShowWindow()
    {
        CreateEnemySet window = EditorWindow.GetWindow<CreateEnemySet>(true, "Enemy Vehicle Set ");
        window.maxSize = new Vector2(500f, 400f);
        window.minSize = window.maxSize;
    }

    void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.Space(20);

        //Title Name
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        normalTextSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = 25;
        GUILayout.Label("Create Enemy Set");
        GUI.skin.label.fontSize = normalTextSize;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        //Vehicle Name Text Field
        GUILayout.BeginHorizontal();
        GUILayout.Label("Set Name", GUILayout.Width(100));
        ObjectName = EditorGUILayout.TextField(ObjectName);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        #region Vehicle 1
        //Vehicle 1
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        normalTextSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = 15;
        GUILayout.Label("Vehicle 1");
        GUI.skin.label.fontSize = normalTextSize;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(5);

        //reference to model1
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Model 1", GUILayout.Width(100));
        vehicleModel1 = (EnemyCarController)EditorGUILayout.ObjectField(vehicleModel1, typeof(EnemyCarController) , false);
        GUILayout.EndHorizontal();

        //Vehicle Direction
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Direction", GUILayout.Width(100));
        vehicle1Dir = EditorGUILayout.Popup(vehicle1Dir, option);
        GUILayout.EndHorizontal();

        //Vehicle Lane
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Lane", GUILayout.Width(100));
        if (vehicle1Dir == 0) //Left
        {
            vehicle1Lane = EditorGUILayout.IntSlider(vehicle1Lane, 1, 4);
        }
        else if (vehicle1Dir == 1) //Right
        {
            vehicle1Lane = EditorGUILayout.IntSlider(vehicle1Lane, 2, 5);
        }
        else if (vehicle1Dir == 2) //Straight
        {
            vehicle1Lane = EditorGUILayout.IntSlider(vehicle1Lane, 1, 5);
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        #endregion

        #region Vehicle 2
        //Vehicle 2
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        normalTextSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = 15;
        GUILayout.Label("Vehicle 2");
        GUI.skin.label.fontSize = normalTextSize;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(5);

        //reference to model2
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Model 2", GUILayout.Width(100));
        vehicleModel2 = (EnemyCarController)EditorGUILayout.ObjectField(vehicleModel2, typeof(EnemyCarController) , false);
        GUILayout.EndHorizontal();

        //Vehicle Direction
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Direction", GUILayout.Width(100));
        vehicle2Dir = EditorGUILayout.Popup(vehicle2Dir, option);
        GUILayout.EndHorizontal();

        //Vehicle Lane
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Lane", GUILayout.Width(100));
        if (vehicle2Dir == 0) //Left
        {
            vehicle2Lane = EditorGUILayout.IntSlider(vehicle2Lane, 1, 4);
        }
        else if (vehicle2Dir == 1) //Right
        {
            vehicle2Lane = EditorGUILayout.IntSlider(vehicle2Lane, 2, 5);
        }
        else if (vehicle2Dir == 2) //Straight
        {
            vehicle2Lane = EditorGUILayout.IntSlider(vehicle2Lane, 1, 5);
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(2);
#endregion

        //Create Btn and Warnings
        GUILayout.Space(10);
        if (ObjectName == "" || vehicleModel1 == null || vehicleModel2 == null)
        {
            EditorGUILayout.HelpBox("You are Missing something", MessageType.Warning);
        }
        else
        {
            if (GUILayout.Button("Create", GUILayout.Height(50)))
            {
                CreateVehicle();
            }
        }

        GUILayout.EndVertical();
    }

    void CreateVehicle()
    {
        var root = new GameObject(ObjectName);
        var rootScript = root.AddComponent<SetController>();

        #region Vehicle 1
        //Vehicle 1
        EnemyCarController vehicle1Obj = Instantiate(vehicleModel1);
        vehicle1Obj.transform.parent = root.transform;
        //position
        if (vehicle1Lane == 1)
        {
            vehicle1Obj.transform.position = new Vector3(1, 0, 0) * 10;
            vehicle1Obj.currentLane = CarLane.one;
        }
        else if (vehicle1Lane == 2)
        {
            vehicle1Obj.transform.position = new Vector3(1, 0, 0) * 5;
            vehicle1Obj.currentLane = CarLane.two;
        }
        else if (vehicle1Lane == 3)
        {
            vehicle1Obj.transform.position = new Vector3(0, 0, 0);
            vehicle1Obj.currentLane = CarLane.three;
        }
        else if (vehicle1Lane == 4)
        {
            vehicle1Obj.transform.position = new Vector3(1, 0, 0) * -5;
            vehicle1Obj.currentLane = CarLane.four;
        }
        else if (vehicle1Lane == 5)
        {
            vehicle1Obj.transform.position = new Vector3(1, 0, 0) * -10;
            vehicle1Obj.currentLane = CarLane.five;
        }
        //move type
        if (vehicle1Dir == 0)
        {
            vehicle1Obj.moveType = MoveType.left;

            if (vehicle1Lane == 1)
            {
                vehicle1Obj.nextLane = CarLane.two;
            }
            else if (vehicle1Lane == 2)
            {
                vehicle1Obj.nextLane = CarLane.three;
            }
            else if (vehicle1Lane == 3)
            {
                vehicle1Obj.nextLane = CarLane.four;
            }
            else if (vehicle1Lane == 4)
            {
                vehicle1Obj.nextLane = CarLane.five;
            }
        }
        else if (vehicle1Dir == 1)
        {
            vehicle1Obj.moveType = MoveType.right;

            if (vehicle1Lane == 5)
            {
                vehicle1Obj.nextLane = CarLane.four;
            }
            else if (vehicle1Lane == 2)
            {
                vehicle1Obj.nextLane = CarLane.one;
            }
            else if (vehicle1Lane == 3)
            {
                vehicle1Obj.nextLane = CarLane.two;
            }
            else if (vehicle1Lane == 4)
            {
                vehicle1Obj.nextLane = CarLane.three;
            }

        }
        else if (vehicle1Dir == 2)
        {
            vehicle1Obj.moveType = MoveType.straight;
        }
        //type
        vehicle1Obj.type = TypeCar.setPart;
        #endregion

        #region Vehicle 2
        //Vehicle 1
        EnemyCarController vehicle2Obj = Instantiate(vehicleModel2);
        vehicle2Obj.transform.parent = root.transform;
        //position
        if(vehicle2Lane == 1)
        {
            vehicle2Obj.transform.position = new Vector3(1, 0, 0) * 10;
            vehicle2Obj.currentLane = CarLane.one;
        }
        else if (vehicle2Lane == 2)
        {
            vehicle2Obj.transform.position = new Vector3(1, 0, 0) * 5;
            vehicle2Obj.currentLane = CarLane.two;
        }
        else if (vehicle2Lane == 3)
        {
            vehicle2Obj.transform.position = new Vector3(0, 0, 0);
            vehicle2Obj.currentLane = CarLane.three;
        }
        else if (vehicle2Lane == 4)
        {
            vehicle2Obj.transform.position = new Vector3(1, 0, 0) * -5;
            vehicle2Obj.currentLane = CarLane.four;
        }
        else if (vehicle2Lane == 5)
        {
            vehicle2Obj.transform.position = new Vector3(1, 0, 0) * -10;
            vehicle2Obj.currentLane = CarLane.five;
        }

        //move type
        if (vehicle2Dir == 0)
        {
            vehicle2Obj.moveType = MoveType.left;

            if (vehicle2Lane == 1)
            {
                vehicle2Obj.nextLane = CarLane.two;
            }
            else if (vehicle2Lane == 2)
            {
                vehicle2Obj.nextLane = CarLane.three;
            }
            else if (vehicle2Lane == 3)
            {
                vehicle2Obj.nextLane = CarLane.four;
            }
            else if (vehicle2Lane == 4)
            {
                vehicle2Obj.nextLane = CarLane.five;
            }
        }
        else if (vehicle2Dir == 1)
        {
            vehicle2Obj.moveType = MoveType.right;

            if (vehicle2Lane == 5)
            {
                vehicle2Obj.nextLane = CarLane.four;
            }
            else if (vehicle2Lane == 2)
            {
                vehicle2Obj.nextLane = CarLane.one;
            }
            else if (vehicle2Lane == 3)
            {
                vehicle2Obj.nextLane = CarLane.two;
            }
            else if (vehicle2Lane == 4)
            {
                vehicle2Obj.nextLane = CarLane.three;
            }

        }
        else if (vehicle2Dir == 2)
            vehicle2Obj.moveType = MoveType.straight;
        //type
        vehicle2Obj.type = TypeCar.setPart;
        #endregion

    }


}
