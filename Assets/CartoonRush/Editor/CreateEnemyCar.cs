﻿using UnityEngine;
using UnityEditor;

public class CreateEnemyCar : EditorWindow
{

    int normalTextSize;
    string ObjectName = "";
    GameObject vehicleModel;

    [MenuItem("Tools/Create Enemy Vehicle")]
    public static void ShowWindow()
    {
        CreateEnemyCar window = EditorWindow.GetWindow<CreateEnemyCar>(true, "Enemy Vehicle");
        window.maxSize = new Vector2(500f, 300f);
        window.minSize = window.maxSize;
    }

    void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.Space(20);

        //Title Name
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        normalTextSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = 25;
        GUILayout.Label("Create Enemy Vehicle");
        GUI.skin.label.fontSize = normalTextSize;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        //Vehicle Name Text Field
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Name", GUILayout.Width(100));
        ObjectName = EditorGUILayout.TextField(ObjectName);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        //reference to model
        GUILayout.BeginHorizontal();
        GUILayout.Label("Vehicle Model", GUILayout.Width(100));
        vehicleModel = (GameObject)EditorGUILayout.ObjectField(vehicleModel, typeof(GameObject) , false);
        GUILayout.EndHorizontal();
        GUILayout.Space(2);

        //Create Btn and Warnings
        GUILayout.Space(10);
        if (ObjectName == "" || vehicleModel == null)
        {
            EditorGUILayout.HelpBox("You are Missing something", MessageType.Warning);
        }
        else
        {
            if (GUILayout.Button("Create", GUILayout.Height(50)))
            {
                CreateVehicle();
            }
        }

        GUILayout.EndVertical();
    }

    void CreateVehicle()
    {
        var root             = new GameObject(ObjectName);
        root.tag             = "Enemy";
        var rootBody         = root.AddComponent<Rigidbody>();
        rootBody.useGravity  = false;
        rootBody.isKinematic = true;
        var boxCollider      = root.AddComponent<BoxCollider>();
        boxCollider.size     = new Vector3(2.6f, 1.8f, 5.2f);
        boxCollider.center   = new Vector3(0, 1.4f, 0);

        var leftIndicator                  = new GameObject("LeftIndicator");
        leftIndicator.transform.parent     = root.transform;
        SpriteRenderer leftSprite          = leftIndicator.AddComponent<SpriteRenderer>();
        leftSprite.color                   = new Color(255f / 255, 219f / 255, 69f / 255, 1);
        leftSprite.sprite                  = Resources.Load("square" , typeof(Sprite)) as Sprite;
        leftIndicator.transform.position   = new Vector3(-0.8f, 0.9f, -2.64f);
        leftIndicator.transform.localScale = new Vector3(0.26f, 0.25f, 1);

        var rightIndicator                 = new GameObject("RightIndicator");
        rightIndicator.transform.parent    = root.transform;
        SpriteRenderer rightSprite         = rightIndicator.AddComponent<SpriteRenderer>();
        rightSprite.color                  = new Color(255f / 255, 219f / 255, 69f / 255, 1);
        rightSprite.sprite                 = Resources.Load("square", typeof(Sprite)) as Sprite;
        rightIndicator.transform.position  = new Vector3(0.8f, 0.9f, -2.64f);
        rightIndicator.transform.localScale = new Vector3(0.26f, 0.25f, 1);

        leftIndicator.SetActive(false);
        rightIndicator.SetActive(false);

        var meshHolder                  = new GameObject("MeshHolder");
        meshHolder.transform.parent     = root.transform;
        GameObject meshModel            = Instantiate(vehicleModel);
        meshModel.transform.parent      = meshHolder.transform;
        meshModel.transform.position    = Vector3.zero;

        var controllerScript            = root.AddComponent<EnemyCarController>();
        controllerScript.leftIndicator  = leftIndicator;
        controllerScript.rightIndicator = rightIndicator;
        controllerScript.laneChgSpeed   = 4;
        controllerScript.explodePower   = 100;
        controllerScript.lanePos        = new int[] { 10, 5, 0, -5, -10 };



    }


}
