﻿using System.Collections;
using UnityEngine;

public enum MoveType { straight, left, right }          //enum to define move of car
public enum CarLane  { one, two, three, four, five }    //enum to define lane of car
public enum TypeCar     { setPart , individual }           //enum to define the car is individual or set of cars

/// <summary>
/// This script controls the vehicles and vehicle set
/// </summary>

public class EnemyCarController : MonoBehaviour {

#region Public
    public GameObject leftIndicator, rightIndicator;              //ref to gameobjects
    public float      laneChgSpeed = 0.4f, explodePower = 100;    //ref to lane change speed and power of explosion
    public int[]      lanePos;                                    //ref to all the lane positions
    public MoveType   moveType;                                   //define the move type
    public bool       changeLane   = false;                       //make the car to change its lane
    public CarLane   currentLane, nextLane;                      //store value of current lane and next lane when car chagnes its lane
    public TypeCar type = TypeCar.individual;                     //which type of car 
#endregion

#region Private
    private GameObject mainCamera;                                                  //ref to camera in the scene
    private int        laneIndex, i = 0;                                            //ref to lane index and "i" which allow to call Coroutine only once
    private float      targetLanePosition, moveSpeed;                               //target lane where car is moving toward , and its move speed
    //changing lane is set true when the car is changing its lane
    //lane changed is set true when the car has changed its lane , means its now in different lane
    private bool       changingLane = false, laneChanged = false;
    private CarLane    defaultLane;                                                 //used by vehicles which are part of set 
    private MoveType   defaultMoveType;                                             //used by vehicles which are part of set 
#endregion


    void Awake()
    {
        defaultMoveType = moveType;             //we keep track of default move type
        defaultLane = currentLane;              //we keep track of default lane
    }

    // Use this for initialization
    void Start ()
    {
        mainCamera      = GameObject.FindGameObjectWithTag("MainCamera");  //ref to camera in the scene
	}
	
	// Update is called once per frame
	void Update ()
    {   //if gameover is false and game started is true
        if (GameManager.instance.gameOver == false && UIManager.instance.GameStarted == true)
        {
            if (type == TypeCar.individual) moveSpeed = LevelController.instance.piecesMoveSpeed;  //if car is individua; we set its speed tp world piece speed
            else if (type == TypeCar.setPart) moveSpeed = 0;                                       //if car is part of set then we set ist speed to zero
                
            //if car moves beyond limit and its and individual object then deactivate it
            if ((transform.position.z - mainCamera.transform.position.z) < -6 && type == TypeCar.individual)
            {
                moveType = MoveType.straight;                                                   //reset its move type
                gameObject.SetActive(false);                                                    //deactivate the gameobject
            }
            else CarMovement();                                                                 //else make it move
        }
	}
    //this method is called by the vehicles which are part of set
    public void DefaultSettings()
    {
        if (type == TypeCar.setPart)                                                               //if its part of set
        {
            moveType = defaultMoveType;                                                         //we reset to default move type
            currentLane = defaultLane;                                                          //we reset to default lane
            i = 0;                                                                              //reset i value
            laneChanged = false;                                                                //reset laneChange bool  
        }

        gameObject.GetComponent<Rigidbody>().useGravity = false;                                //make gravity disable
        gameObject.GetComponent<Rigidbody>().isKinematic = true;                                //make it kinematic

    }

    //this method controls the car movement on z axis and on x axis
    void CarMovement()
    {   //if car has to change its lane , move type is not straight and lane changing process is not complete
        if (changeLane == true && moveType != MoveType.straight && laneChanged == false)
        {
            changeLane   = false;                   //make set it false as we want to call this code only once

            if (moveType == MoveType.left)          //if movetype is left we make it move left side
                    MovingLeftSide();

            else if (moveType == MoveType.right)    //if movetype is left we make it move left side
                MovingRightSide();
           
        }

        if (changingLane == false)                  //if changing lane is false we move in straight line
        {
            transform.Translate(
                new Vector3(
                    0, //x axis
                    0, //y axis
                    -1 * GameManager.instance.currentPlayer.ZMoveSpeed * moveSpeed * Time.deltaTime * 0.75f) //z axis
                    );
        }
        else if (changingLane == true)              //if changing lane is true , we see move type
        {
            switch (moveType)
            {   //if its left we move left
                case MoveType.left:
                    if ((transform.position.x - targetLanePosition) > 0)    //if difference between car position and target lane is more than zero
                    {
                        if (i == 0)                                         //we check for "i" values
                        {
                            i++;                                            //increase it by 1
                            StartCoroutine(BlinkIndicator(moveType));       //start indicator blinking
                        }
                        transform.Translate(                                //and start the car to move
                        new Vector3(
                            -1 * Time.deltaTime * laneChgSpeed, //x axis
                            0, //y axis
                            -1 * GameManager.instance.currentPlayer.ZMoveSpeed * moveSpeed * Time.deltaTime * 0.75f) //z axis
                            );
                    }
                    else if ((transform.position.x - targetLanePosition) <= 0) //if difference between car position and target lane is less than zero
                    {
                        laneChanged = true;                                 //lane change process is completed
                        changingLane = false;                               //car is no more changing its lane so set it to false
                        moveType     = MoveType.straight;                   //move type is changed to straight
                        leftIndicator.SetActive(false);                     //indicator is deactivated
                    }
                    break;
                //if its right we move right
                case MoveType.right:
                    if ((transform.position.x - targetLanePosition) < 0)    //if difference between car position and target lane is less than zero
                    {
                        if (i == 0)                                         //we check for "i" values
                        {
                            i++;                                            //increase it by 1
                            StartCoroutine(BlinkIndicator(moveType));       //start indicator blinking
                        }
                        transform.Translate(                                //and start the car to move
                        new Vector3(
                            1 * Time.deltaTime * laneChgSpeed, //x axis
                            0, //y axis
                            -1 * GameManager.instance.currentPlayer.ZMoveSpeed * moveSpeed * Time.deltaTime * 0.75f) //z axis
                            );
                    }
                    else if ((transform.position.x - targetLanePosition) > 0)  //if difference between car position and target lane is more than zero
                    {
                        laneChanged = true;                                 //lane change process is completed
                        changingLane = false;                               //car is no more changing its lane so set it to false
                        moveType     = MoveType.straight;                   //move type is changed to straight
                        rightIndicator.SetActive(false);                    //indicator is deactivated
                    }
                    break;
            }
            
        }
    }

    //this decide the next lane position depending on car lane
    void ChangeLane()
    {
        switch (nextLane)
        {
            case CarLane.one:
                targetLanePosition = lanePos[0];
                break;
            case CarLane.two:
                targetLanePosition = lanePos[1];
                break;
            case CarLane.three:
                targetLanePosition = lanePos[2];
                break;
            case CarLane.four:
                targetLanePosition = lanePos[3];
                break;
            case CarLane.five:
                targetLanePosition = lanePos[4];
                break;
        }

        changingLane = true;
    }

    //when car is moving left
    public void MovingLeftSide()
    {  
        if (GameManager.instance.gameOver == false)     //we check if game is over or not
        {   
            switch (currentLane)                        //then we check the current lane
            {
                case CarLane.five:                      //if current lane is five which is extreme left lane , we do nothing
                    changingLane = false;
                    break;
                case CarLane.four:                      //if current lane is four , we change nextLane lane to five which is on left side on fourth lane
                    nextLane = CarLane.five;
                    ChangeLane();                       //ChangeLane method is called so we can set the target
                    break;
                case CarLane.three:
                    nextLane = CarLane.four;
                    ChangeLane();
                    break;
                case CarLane.two:
                    nextLane = CarLane.three;
                    ChangeLane();
                    break;
                case CarLane.one:
                    nextLane = CarLane.two;
                    ChangeLane();
                    break;
            }
        }
    }

    //when car is moving right
    public void MovingRightSide()
    {   //we check if game is over or not
        if (GameManager.instance.gameOver == false)
        {   //the we check the current lane
            switch (currentLane)
            {
                case CarLane.five: //if current lane is five which is extreme left lane , we change nextLane lane to four which is on right side on fifth lane
                    nextLane = CarLane.four;
                    ChangeLane();
                    break;
                case CarLane.four:
                    nextLane = CarLane.three;
                    ChangeLane();
                    break;
                case CarLane.three:
                    nextLane = CarLane.two;
                    ChangeLane();
                    break;
                case CarLane.two:
                    nextLane = CarLane.one;
                    ChangeLane();
                    break;
                case CarLane.one: //if current lane is one which is extreme right lane , we do nothing
                    changingLane = false;
                    break;
            }
        }
    }

    //this make the indicator blink
    IEnumerator BlinkIndicator(MoveType type)
    {
        if (moveType == MoveType.left)                  //if move type is left
        {
            leftIndicator.SetActive(true);              //we activate left indicator
            yield return new WaitForSeconds(0.25f);     //after 0.25f sec
            leftIndicator.SetActive(false);             //we deactivate left indicator
        }
        else if (moveType == MoveType.right)            //if move type is right
        {       
            rightIndicator.SetActive(true);             //we activate right indicator
            yield return new WaitForSeconds(0.25f);     //after 0.25f sec
            rightIndicator.SetActive(false);            //we deactivate left indicator
        }

        yield return new WaitForSeconds(0.25f);         //after 0.25f sec

        StartCoroutine(BlinkIndicator(moveType));       //we call this Coroutine again

    }

    //this method make the car explode whne the player car crashed
    public void Explode()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = false;   //we set kinematic to false
                                                                    //add some force to the vehicle
        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-explodePower, explodePower), 
                                                                  Random.Range(explodePower * 2, explodePower * 3), 
                                                                  Random.Range(-explodePower, explodePower)));
                                                                    //add some torge to the vehicle
        gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-explodePower, explodePower), 
                                                                   Random.Range(explodePower * 2, explodePower * 3), 
                                                                   Random.Range(-explodePower, explodePower)));
        gameObject.GetComponent<Rigidbody>().useGravity = true;     //set garvity true
    }

}
