﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script create clones of repeting gameobject and store it in a List which then used in the game
/// </summary>

public class ObjectPooling : MonoBehaviour {

    public static ObjectPooling instance;   //me make it an instance so it become easy for us to get its reference in other scripts

    public int        totalSpawnCount = 2;                              //gameobject clone spawned at start of game
    public GameObject coinSet1, coinSet2, coinSet3, coinEffect;         //ref to coin set prefabs
    public GameObject car1, car2, taxi, pickUpCar, van2, truck, van;   //ref to vehicle individual prefab
    public GameObject set1, set2, set3, set4, set5;    //ref to vehicle set prefabs

    #region Individual                                                  
    List<GameObject> CoinFxList       = new List<GameObject>();         //we create empty list for each prefab
    List<GameObject> CoinSet1List     = new List<GameObject>();
    List<GameObject> CoinSet2List     = new List<GameObject>();
    List<GameObject> CoinSet3List     = new List<GameObject>();
    List<GameObject> Car1List         = new List<GameObject>();
    List<GameObject> Car2List         = new List<GameObject>();
    List<GameObject> TaxiList         = new List<GameObject>();
    List<GameObject> Van2List    = new List<GameObject>();
    List<GameObject> VanList          = new List<GameObject>();
    List<GameObject> TruckList        = new List<GameObject>();
    List<GameObject> PickUpCarList    = new List<GameObject>();
    #endregion

    #region Set
    List<GameObject> Set1List = new List<GameObject>();                 //we create empty list for each prefab
    List<GameObject> Set2List = new List<GameObject>();
    List<GameObject> Set3List = new List<GameObject>();
    List<GameObject> Set4List = new List<GameObject>();
    List<GameObject> Set5List = new List<GameObject>();
    #endregion

    void Awake()
    {
        if (instance == null)
            instance = this;

        #region Creating Clones
        //coinsFx
        for (int i = 0; i < totalSpawnCount; i++)           //we create clones of each prefab
        {
            GameObject obj = Instantiate(coinEffect);       //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            CoinFxList.Add(obj);                            //and add to list
        }

        //coinSet1
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(coinSet1);         //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            CoinSet1List.Add(obj);                          //and add to list
        }

        //coinSet2
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(coinSet2);         //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            CoinSet2List.Add(obj);                          //and add to list
        }

        //coinSet3
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(coinSet3);         //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            CoinSet3List.Add(obj);                          //and add to list
        }

        //car1
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(car1);             //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            Car1List.Add(obj);                              //and add to list
        }

        //car2
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(car2);             //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            Car2List.Add(obj);                              //and add to list
        }

        //taxi
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(taxi);             //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            TaxiList.Add(obj);                              //and add to list
        }

        //Van2
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(van2);        //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            Van2List.Add(obj);                         //and add to list
        }

        //truck
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(truck);            //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            TruckList.Add(obj);                             //and add to list
        }

        //van
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(van);              //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            VanList.Add(obj);                               //and add to list
        }

        //PickUp Car
        for (int i = 0; i < totalSpawnCount; i++)
        {
            GameObject obj = Instantiate(pickUpCar);        //we spawn the clone
            obj.transform.parent = gameObject.transform;    //set its parent
            obj.SetActive(false);                           //make it deactive
            PickUpCarList.Add(obj);                         //and add to list
        }

        #endregion

    }

    #region Individual
    //this method is called when we need to spawn a coin FX
    public GameObject GetCoinFx()
    {
        for (int i = 0; i < CoinFxList.Count; i++)
        {
            if (!CoinFxList[i].activeInHierarchy)
                return CoinFxList[i];
        }

        GameObject obj = Instantiate(coinEffect);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        CoinFxList.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a coinset1
    public GameObject GetCoinSet1()
    {
        for (int i = 0; i < CoinSet1List.Count; i++)
        {
            if (!CoinSet1List[i].activeInHierarchy)
                return CoinSet1List[i];
        }

        GameObject obj = Instantiate(coinSet1);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        CoinSet1List.Add(obj);

        return obj;
    }
    //this method is called when we need to spawn a coin set2
    public GameObject GetCoinSet2()
    {
        for (int i = 0; i < CoinSet2List.Count; i++)
        {
            if (!CoinSet2List[i].activeInHierarchy)
                return CoinSet2List[i];
        }

        GameObject obj = Instantiate(coinSet2);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        CoinSet2List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a coin set3
    public GameObject GetCoinSet3()
    {
        for (int i = 0; i < CoinSet3List.Count; i++)
        {
            if (!CoinSet3List[i].activeInHierarchy)
                return CoinSet3List[i];
        }

        GameObject obj = Instantiate(coinSet3);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        CoinSet3List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a car
    public GameObject GetCar1()
    {
        for (int i = 0; i < Car1List.Count; i++)
        {
            if (!Car1List[i].activeInHierarchy)
                return Car1List[i];
        }

        GameObject obj = Instantiate(car1);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Car1List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a car2
    public GameObject GetCar2()
    {
        for (int i = 0; i < Car2List.Count; i++)
        {
            if (!Car2List[i].activeInHierarchy)
                return Car2List[i];
        }

        GameObject obj = Instantiate(car2);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Car2List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a taxi
    public GameObject GetTaxi()
    {
        for (int i = 0; i < TaxiList.Count; i++)
        {
            if (!TaxiList[i].activeInHierarchy)
                return TaxiList[i];
        }

        GameObject obj = Instantiate(taxi);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        TaxiList.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a firetruck
    public GameObject GetVan2()
    {
        for (int i = 0; i < Van2List.Count; i++)
        {
            if (!Van2List[i].activeInHierarchy)
                return Van2List[i];
        }

        GameObject obj = Instantiate(van2);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Van2List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a van
    public GameObject GetVan()
    {
        for (int i = 0; i < VanList.Count; i++)
        {
            if (!VanList[i].activeInHierarchy)
                return VanList[i];
        }

        GameObject obj = Instantiate(van);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        VanList.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a truck
    public GameObject GetTruck()
    {
        for (int i = 0; i < TruckList.Count; i++)
        {
            if (!TruckList[i].activeInHierarchy)
                return TruckList[i];
        }

        GameObject obj = Instantiate(truck);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        TruckList.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a Ambulance
    public GameObject GetPickUpCar()
    {
        for (int i = 0; i < PickUpCarList.Count; i++)
        {
            if (!PickUpCarList[i].activeInHierarchy)
                return PickUpCarList[i];
        }

        GameObject obj = Instantiate(pickUpCar);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        PickUpCarList.Add(obj);

        return obj;
    }

    #endregion

    #region Set

    //this method is called when we need to spawn a set1
    public GameObject GetSet1()
    {
        for (int i = 0; i < Set1List.Count; i++)
        {
            if (!Set1List[i].activeInHierarchy)
                return Set1List[i];
        }

        GameObject obj = Instantiate(set1);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Set1List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a set2
    public GameObject GetSet2()
    {
        for (int i = 0; i < Set2List.Count; i++)
        {
            if (!Set2List[i].activeInHierarchy)
                return Set2List[i];
        }

        GameObject obj = Instantiate(set2);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Set2List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a set3
    public GameObject GetSet3()
    {
        for (int i = 0; i < Set3List.Count; i++)
        {
            if (!Set3List[i].activeInHierarchy)
                return Set3List[i];
        }

        GameObject obj = Instantiate(set3);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Set3List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a set4
    public GameObject GetSet4()
    {
        for (int i = 0; i < Set4List.Count; i++)
        {
            if (!Set4List[i].activeInHierarchy)
                return Set4List[i];
        }

        GameObject obj = Instantiate(set4);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Set4List.Add(obj);

        return obj;
    }

    //this method is called when we need to spawn a set5
    public GameObject GetSet5()
    {
        for (int i = 0; i < Set5List.Count; i++)
        {
            if (!Set5List[i].activeInHierarchy)
                return Set5List[i];
        }

        GameObject obj = Instantiate(set5);
        obj.transform.parent = gameObject.transform;
        obj.SetActive(false);
        Set5List.Add(obj);

        return obj;
    }

    #endregion

}
