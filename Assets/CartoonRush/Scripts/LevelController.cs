﻿using UnityEngine;

/// <summary>
/// This script keep track of all the level piece transform , which active and loop them
/// </summary>

public class LevelController : MonoBehaviour {

    public static LevelController instance;             //me make it an instance so it become easy for us to get its reference in other scripts

    public float        piecesMoveSpeed = 4;            //speed with which the pieces moves
    public GameObject[] levelParts;                     //Array of all the parts in the scene
    public Transform    lastGameObj;                    //ref to last peice

    [SerializeField] private int nextPartToActive;      //ref to index of next element to activate

    private GameObject currentPart;                     //part on which player car is moving
    private GameObject playerTarget;                    //ref to player

    public GameObject CurrentPart { set { currentPart = value; } }  //setter

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Update()
    {    //if game is over or game is not started we return from Update method
        if (GameManager.instance.gameOver == true || UIManager.instance.GameStarted == false) return;

        if (currentPart != null)                                            //if current part in not null
        {
            if ((currentPart.transform.position.z - playerTarget.transform.position.z) < -27) //and the current part has passed player by distance more than 27
            {   
                //we change the current part position and position it after the last part
                currentPart.transform.position = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y, lastGameObj.position.z + 40);
                currentPart.gameObject.SetActive(false);                    //deactivate it
            }                       
        }
    }

    //this method call all the pieces and make them to move
    public void StartMoving()
    {
        playerTarget = GameManager.instance.currentPlayer.gameObject; //get reference to the player

        if (playerTarget != null)                                     //if playerTarget is not null
        {
            for (int i = 0; i < levelParts.Length; i++)               //we loop through all the pieces
            {                                                         // we asign player target to pieces
                levelParts[i].GetComponent<LevelPieceController>().player = playerTarget.GetComponent<PlayerController>();
                levelParts[i].GetComponent<LevelPieceController>().startMoving = true;  //set it to move
            }
        }
    }

    //this method is called by player when it collide with the level piece collider
    public void ChangeThePartPos()
    {
        levelParts[nextPartToActive].SetActive(true); //we activate next part
        //set its transform
        levelParts[nextPartToActive].transform.position = new Vector3(currentPart.transform.position.x, currentPart.transform.position.y, lastGameObj.position.z + 40);
        lastGameObj                                     = levelParts[nextPartToActive].transform;

        if (nextPartToActive < levelParts.Length - 1) nextPartToActive++; //change the nextpiece index
        else                                          nextPartToActive = 0;
    }
}
