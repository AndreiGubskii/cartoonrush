﻿using UnityEngine;

/// <summary>
/// This script spawn the selected player vehicle in the scene
/// </summary>

public class PlayerCarSpawner : MonoBehaviour {

    public static PlayerCarSpawner instance;    //me make it an instance so it become easy for us to get its reference in other scripts

    [Tooltip("Drag the Prefabs of Player vehicles")]
    public GameObject[] playerCars;             //ref to all the player vehicles prefabs

    void Awake()
    {
        if (instance == null)
            instance = this;

        SpawnTheCar();                          //at start we spawn the selected car
    }

    //this method spawn the selected car and remove any car which is not selected , this method is also called by shop script
    public void SpawnTheCar()
    {
        GameObject lastPlayer = GameObject.FindGameObjectWithTag("Player");     //get reference to car with player tag in scene
        if (lastPlayer != null) Destroy(lastPlayer);                            //destroy it

        GameObject newPlayer = Instantiate(playerCars[GameManager.instance.selectedSkin]); //spawn the selected car
        newPlayer.transform.parent = transform;                                            //set its parent
        newPlayer.transform.position = new Vector3(0, 0, -12.6f);                          //set its transform

        GameManager.instance.currentPlayer = newPlayer.GetComponent<PlayerController>();   //and add reference to GameManager
    }

}
