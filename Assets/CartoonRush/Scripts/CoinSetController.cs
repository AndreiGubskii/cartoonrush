﻿using UnityEngine;

/// <summary>
/// This script controls the coin set
/// </summary>

public class CoinSetController : MonoBehaviour {

    [SerializeField] private GameObject[] childrens; //ref to all the coins which are children of this gameobject 
	
	// Update is called once per frame
	void Update ()
    {   
        //if game is over or game is not started we return from Update method
        if (GameManager.instance.gameOver == true || UIManager.instance.GameStarted == false) return;
        //we make the set to move along -ve z axis depending on player ZSpeed , world piece speed w.r.t Time
        transform.Translate(-Vector3.forward * GameManager.instance.currentPlayer.ZMoveSpeed * LevelController.instance.piecesMoveSpeed * Time.deltaTime);
    }

    //this method is called when this gamobject is make active in the scene
    public void DefaultSettings()
    {   
        for (int i = 0; i < childrens.Length; i++)  //for loop is called
        {   
            childrens[i].SetActive(true);           //and all the children are make active
        }
    }
}
