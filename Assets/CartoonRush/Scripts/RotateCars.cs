﻿using UnityEngine;

/// <summary>
/// This script rotates vehicle in the shop
/// </summary>

public class RotateCars : MonoBehaviour {

    [Header("Drag the children of CarHolder into the fields in sequence")]
    public float speed = 5f;        //rotation speed
    [SerializeField]
    private GameObject[] cars;      //ref to all the cars in shop


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < cars.Length; i++)       //we loop thorugh all the cars
        {                                           //rotate them on Y axis
            cars[i].transform.RotateAround(cars[i].transform.position, cars[i].transform.up, Time.deltaTime * speed);
        }

    }
}
