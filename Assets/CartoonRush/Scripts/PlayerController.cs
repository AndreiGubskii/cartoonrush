﻿using UnityEngine;
using System.Collections;

public enum PlayerLane      { one, two, three, four, five }  //enum to define lane of car

[RequireComponent(typeof(Rigidbody))]               //required component
public class PlayerController : MonoBehaviour {

    public static PlayerController instance;        //me make it an instance so it become easy for us to get its reference in other scripts

    #region Public Variables
    public AudioClip    swipeSound, crashSound, coinPickUp;    //ref to audio clips
    public float        carSpeed;                              //ref to car speed
    public GameObject   explosionHolder;                       //ref to explosion particles
    public Material     explosionMat;                          //ref to explosion material of car
    public Renderer     carRenderer;                           //ref to renderer
  
    [Header("Lane Settings")]
    public float        turnSpeed;                             //speed with which the car is turn to left or right
    public float[]      limits, lanePos;                       //road limits and each lane positions
    public PlayerLane   currentLane;                           //current lane on which car is
    public float        targetLanePosition;                    //target lane where car has to move

    [Header("Speed Multiplier Settings")]
    public int          levelUpScore           = 100;          //score milestone for level up
    internal int        increaseCount          = 0;            //this is used to keep track it the milestone is achieved or not
    public float        levelUpSpeedMultiplier = 0.1f;         //amount by which speed changes

    [Space(10)]
    public Animator     carAnim;
    public float        isDoubleSpeed          = 1;            //used for nitros
    public float        brakeSpeed             = 1;            //to activate brake
    [HideInInspector]
    public bool         braking = false, acceleration = false; //bool to keep track when player apply brake and acceleration
    #endregion

    #region Private Variables  
    private float       zMoveSpeed;                 //variable used by other gameobject to move on Z axis
    private Rigidbody   myBody;                     //ref to rigidbody of vehicle
    #endregion


    public float ZMoveSpeed                         //getter
    {
        get { return zMoveSpeed; }
    }

    void OnEnable()                                 //when the scene is started
    {
        isDoubleSpeed = 1;                          //set default value
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
        myBody = GetComponent<Rigidbody>();         //get reference to component attached
    }

    // Use this for initialization
    void Start ()
    {
        increaseCount = levelUpScore;               //set the increase count
    }
	
	// Update is called once per frame
	void Update ()
    {   //if game is not over or game is started
        if (GameManager.instance.gameOver == false && UIManager.instance.GameStarted == true)
        {
            IncreaseSpeed();                        //we keep track of speed so we can increse it at requrired milestone
            PlayerInput();                          //keep track of player input
        }
    }

    void FixedUpdate()
    {   //if game is not over or game is started
        if (GameManager.instance.gameOver == false && UIManager.instance.GameStarted == true)
        {
            PlayerMovement();                       //make player move        CHANGED CODE
        }
    }

    void PlayerInput()
    {   
#if UNITY_EDITOR || UNITY_WEBPLAYER
        //if player press down arrow/S key/Space Bar the we apply brakes
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Space) || braking == true)
            brakeSpeed = 2;
        else
            brakeSpeed = 1;

        //if player press Up arrow/W key the we apply acceleration
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || acceleration == true)
            isDoubleSpeed = 2;
        else
            isDoubleSpeed = 1;

        if (transform.position.x >= -10 && transform.position.x <= 10)
        {
            KeyboardControl();
        }
#endif

    }

    void KeyboardControl()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            MovingLeftSide();

        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            MovingRightSide();
    }

    void PlayerMovement()
    {
        zMoveSpeed = (carSpeed * isDoubleSpeed) / brakeSpeed;
        //Increase distance
        GameManager.instance.currentDistance += (carSpeed * Time.deltaTime); //CHANGED CODE
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, targetLanePosition, turnSpeed), transform.position.y, transform.position.z);
    }

    //depending on current lane , target lane position is assigned
    void PlayerLaneChange()
    {
        switch (currentLane)
        {
            case PlayerLane.one:
                targetLanePosition = lanePos[0];
                break;
            case PlayerLane.two:
                targetLanePosition = lanePos[1];
                break;
            case PlayerLane.three:
                targetLanePosition = lanePos[2];
                break;
            case PlayerLane.four:
                targetLanePosition = lanePos[3];
                break;
            case PlayerLane.five:
                targetLanePosition = lanePos[4];
                break;
        }
    }

    //when car is moving left
    public void MovingLeftSide()
    {   //we check if game is over or not
        if (GameManager.instance.gameOver == false)
        {   //the we check the current lane
            switch (currentLane)
            {
                case PlayerLane.five: //if current lane is five which is extreme left lane , we do nothing
                    break;
                case PlayerLane.four: //if current lane is four , we change current lane to five which is on left side on fourth lane
                    currentLane = PlayerLane.five;
                    PlayAnimation("TurnLeft");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.three:
                    currentLane = PlayerLane.four;
                    PlayAnimation("TurnLeft");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.two:
                    currentLane = PlayerLane.three;
                    PlayAnimation("TurnLeft");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.one:
                    currentLane = PlayerLane.two;
                    PlayAnimation("TurnLeft");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
            }
            PlayerLaneChange();                                                                                                 //CHANGED CODE
        }
    }

    //when car is moving right 
    public void MovingRightSide()
    {   //we check if game is over or not
        if (GameManager.instance.gameOver == false)
        {   //the we check the current lane
            switch (currentLane)
            {
                case PlayerLane.five: //if current lane is five which is extreme left lane , we change current lane to four which is on right side on fifth lane
                    currentLane = PlayerLane.four;
                    PlayAnimation("TurnRight");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.four: 
                    currentLane = PlayerLane.three;
                    PlayAnimation("TurnRight");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.three:
                    currentLane = PlayerLane.two;
                    PlayAnimation("TurnRight");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.two:
                    currentLane = PlayerLane.one;
                    PlayAnimation("TurnRight");
                    UIManager.instance.audioS.PlayOneShot(swipeSound);
                    break;
                case PlayerLane.one: //if current lane is one which is extreme right lane , we do nothing
                    break;
            }
            PlayerLaneChange();                                                                                     //CHANGED CODE
        }
    }

    public void IncreaseSpeed() //this increase the player speed
    {   //we check if the distance travelled by the player is more or equal to milestone
        if (Mathf.Round(GameManager.instance.currentDistance) > increaseCount)
        {
            increaseCount += levelUpScore;  //then we increse the next milestone
            levelUpScore += levelUpScore;   //increse the levelUpScore
            carSpeed += carSpeed * levelUpSpeedMultiplier;  //increase the speed
        }
    }

    //this method is called to play the specific animations on the car
    void PlayAnimation(string animState)
    {
        carAnim.Play(animState);            
    }

    //when car collide with other objects , this method is called
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LevelSet"))                                                   //when it collide with level piece
        {   
            other.GetComponentInParent<LevelController>().CurrentPart = other.gameObject;   //levelcontroller currentPart is set
            other.GetComponentInParent<LevelController>().ChangeThePartPos();               //levelcontroller method is called
        }

        if (other.CompareTag("Enemy"))                                                      //when it collide with enemy car
        {
            UIManager.instance.audioS.PlayOneShot(crashSound);                              //we play crash sound
            StartCoroutine(Wait(other.gameObject));
        }

        if (other.CompareTag("Coin"))                                                       //when it collide with coin
        {
            UIManager.instance.audioS.PlayOneShot(coinPickUp);                              //we play pickup sound
            GameManager.instance.currentCoinsEarned++;                                      //increase the currentCoin variable by 1
            GameManager.instance.coinAmmount++;                                             //increase total coins by 1
            other.gameObject.SetActive(false);                                              //deactivate it
            GameManager.instance.Save();                                                    //save it
            UIManager.instance.coinText.text = "" + GameManager.instance.currentCoinsEarned; //update the coin text in game

            //Below code is to play coin effect , the effect I have is not good so uncommented it.

            //GameObject coinFx = ObjectPooling.instance.GetCoinFx();
            //coinFx.transform.position = other.transform.position + Vector3.up;
            //coinFx.SetActive(true);
            //StartCoroutine(coinFx.GetComponent<DeactivateObj>().Deactivate());
        }
    }

    IEnumerator Wait(GameObject other)
    {
        yield return new WaitForEndOfFrame();
        carRenderer.material = explosionMat;                                            //set the car renderer material
        explosionHolder.SetActive(true);                                                //activate explosion
        PlayAnimation("Explosion");                                                     //play explosion anmiation
        other.GetComponent<EnemyCarController>().Explode();                             //make other car explode
        
        StartCoroutine(UIManager.instance.GameOver());                                  //and game over
    }

}
