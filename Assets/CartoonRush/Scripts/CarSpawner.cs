﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script spawn the enemy vehicle , coin on different lanes
/// </summary>

public class CarSpawner : MonoBehaviour {

    [SerializeField] private GameObject[] spawnLanePos;                  //ref to all the lanes in the scene
    [SerializeField] private List<int>    randomInts = new List<int>();  //random int list used to spawn vehicles on random lanes

    private PlayerController player;                                     //variable to store player
    private float            spawnWaitTime;                              //time to wait before spawn
    private float            currentWaitTime;                            //current wait time which is running                
    private int              totalToSpawn;                               //total vehicles to spawn at one time
	
	// Update is called once per frame
	void Update ()
    {
        //if game is over or game is not started we return from Update method
        if (GameManager.instance.gameOver == true || UIManager.instance.GameStarted == false) return;

        if (player == null || player != GameManager.instance.currentPlayer) //if player variable is empty or its not same as gamemanager variable
            player = GameManager.instance.currentPlayer;                    //we reset the player variable

        currentWaitTime -= Time.deltaTime;                                  //currentWaitTime is reduce each sec

        if (currentWaitTime <= 0)                                           //when the wait time is leass than or equal to Zero
        {
            RandomNumber();                                                 //we create some random numbers

            int r = Random.Range(0, 5);                                     //then we get a random number

            if      (r == 2) SpawnVehiclesSet();                            //if r is 2 we spawn a Set of cars                           
            else if (r == 4) SpawnCoins();                                  //if r is 4 we spawn a Set of coins
            else                                                            //else we spawn a car
            {
                for (int i = 0; i < totalToSpawn; i++)                      //depending on totalToSpawn int we spawn the number of cars
                {
                    SpawnVehicles(i);
                }
            }
            
        }
	}

    //method which spawn a set of cars(set of cars a the cars which change there lanes)
    void SpawnVehiclesSet()
    {
        currentWaitTime = Random.Range(2 * 2 / player.carSpeed, 3 * 2 / player.carSpeed); //we set the currentWaitTime 
        GameObject setObj = null;                                                         //we create a gameobject variable

        int chooseVehicle = Random.Range(0, 5);                                           //we choose a random number

        //and depending on random number we assign the vehicle gamobject to the variable
        if (chooseVehicle == 0)      setObj = ObjectPooling.instance.GetSet1();           //we call objectpooling script method here
        else if (chooseVehicle == 1) setObj = ObjectPooling.instance.GetSet2();
        else if (chooseVehicle == 2) setObj = ObjectPooling.instance.GetSet3();
        else if (chooseVehicle == 3) setObj = ObjectPooling.instance.GetSet4();
        else if (chooseVehicle == 4) setObj = ObjectPooling.instance.GetSet5();

        setObj.transform.position           = transform.position;                         //we then set the transform of the object

        setObj.SetActive(true);                                                           //make it active in the scene
        setObj.GetComponent<SetController>().DefaultSettings();                           //we call DefaultSettings method from the script attach on object

    }

    //this method spawn the coin set
    void SpawnCoins()
    {       
        currentWaitTime = 2 * 2 / player.carSpeed;                                        //we set the currentWaitTime    
        GameObject coinObj = null;                                                        //we create a gameobject variable

        int chooseCoinSet = Random.Range(0, 3);                                           //we choose a random number

        //and depending on random number we assign the coin gamobject to the variable
        if (chooseCoinSet == 0)      coinObj = ObjectPooling.instance.GetCoinSet1();      //we call objectpooling script method here
        else if (chooseCoinSet == 1) coinObj = ObjectPooling.instance.GetCoinSet2();
        else if (chooseCoinSet == 2) coinObj = ObjectPooling.instance.GetCoinSet3();
        coinObj.transform.position           = transform.position;                        //we then set the transform of the object

        coinObj.SetActive(true);                                                          //make it active in the scene

        coinObj.GetComponent<CoinSetController>().DefaultSettings();                      //we call DefaultSettings method from the script attach on object
    }

    //this method spawn the car
    void SpawnVehicles(int i)
    {   
        currentWaitTime = Random.Range(2 * 2 / player.carSpeed, 3 * 2 / player.carSpeed); //we set the currentWaitTime
        GameObject vehicleObj = null;                                                     //we create a gameobject variable

        int chooseVehicle = Random.Range(0, 7);                                           //we choose a random number

        //and asign the vehicle gamobject to the variable   
        if (chooseVehicle == 0)      vehicleObj = ObjectPooling.instance.GetCar1();       //we call objectpooling script method here
        else if (chooseVehicle == 1) vehicleObj = ObjectPooling.instance.GetPickUpCar();
        else if (chooseVehicle == 2) vehicleObj = ObjectPooling.instance.GetVan2();
        else if (chooseVehicle == 3) vehicleObj = ObjectPooling.instance.GetTruck();
        else if (chooseVehicle == 4) vehicleObj = ObjectPooling.instance.GetTaxi();
        else if (chooseVehicle == 5) vehicleObj = ObjectPooling.instance.GetVan();
        else if (chooseVehicle == 6) vehicleObj = ObjectPooling.instance.GetCar2();

        int laneIndex = randomInts[i];                                                    //get the random lane

        vehicleObj.transform.position = spawnLanePos[laneIndex].transform.position;       //transform object position to that lane

        vehicleObj.SetActive(true);                                                       //make the object active
        vehicleObj.GetComponent<EnemyCarController>().DefaultSettings();                  //we call DefaultSettings method from the script attach on object

    }

    //this method create a random number
    void RandomNumber()
    {   //1st we decide how many car we need to spawn
        totalToSpawn = Random.Range(1, 4);                                                //so we can get nuber between 1 - 4 (excluding 4)

        if (totalToSpawn == 1) randomInts[0] = Random.Range(0, spawnLanePos.Length);     //depending on totalToSpawn we assign values to random number list

        else if (totalToSpawn == 2)
        {
            randomInts[0] = Random.Range(0, spawnLanePos.Length);                        //we assing value to 1st element

            randomInts[1] = Random.Range(0, spawnLanePos.Length);                        //we assing value to 2st element
            while (randomInts[1] == randomInts[0])                                       //check if 1st and 2nd are same
            {
                randomInts[1] = Random.Range(0, spawnLanePos.Length);                    //if yes we keep generating random number untill its different
            }
        }

        else if (totalToSpawn == 3)
        {
            randomInts[0] = Random.Range(0, spawnLanePos.Length);                        //we assing value to 1st element

            randomInts[1] = Random.Range(0, spawnLanePos.Length);                        //we assing value to 2st element
            while (randomInts[1] == randomInts[0])                                       //check if 1st and 2nd are same
            {
                randomInts[1] = Random.Range(0, spawnLanePos.Length);                    //if yes we keep generating random number untill its different
            }

            randomInts[2] = Random.Range(0, spawnLanePos.Length);                        //we assing value to 3rd element
            while (randomInts[2] == randomInts[0] || randomInts[2] == randomInts[1])     //check if 3rd and 1st or 3rd and 2nd are same
            {
                randomInts[2] = Random.Range(0, spawnLanePos.Length);                    //if yes we keep generating random number untill its different
            }
        }

    }

}
