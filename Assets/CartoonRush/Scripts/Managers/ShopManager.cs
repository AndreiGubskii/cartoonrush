﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script controls shop menu
/// </summary>

[System.Serializable]
public class ItemData       //class which stores the data of vehicles
{
    public string name;     //vehicle name
    public int cost;        //vehicle cost
    [HideInInspector]
    public bool unlocked;   //vehicle is unlocked or not
}

public class ShopManager : MonoBehaviour {

    public static ShopManager instance;       //me make it an instance so it become easy for us to get its reference in other scripts

    public GameObject lockObj;                //ref to lock img object in the shop
    public Transform  carHolder;              //gameobject which holds all the cars
    public float      speed = 5;              //speed with which we move car holder
    public Text       vehicleName, coinText;  //text object of name and cost
    public Text       selectBtnText;          //text object of select btn

    public ItemData[] itemData;               //array of items (cars)

    private int       currentVehicle = 0;     //ref to which vehicle is shown in shop
    private bool      scrolling = false;      //this decide if we are scrolling or not

    private float     nextPos;                //keep track of next position , when we swipe 


    void Awake()
    {
        if (instance == null) instance = this;
    }

    void Start()
    {
        coinText.text = "Coins: " + GameManager.instance.coinAmmount;        //update the coin text
    }

	// Update is called once per frame
	void Update ()
    {   //we set the car holder position
        carHolder.position = new Vector3(carHolder.position.x, carHolder.position.y, Mathf.Lerp(carHolder.transform.position.z, nextPos, speed));

        //if difference of carholder position and target pos is less than 0.05f
        if (Mathf.Abs(carHolder.transform.position.z - nextPos) <= 0.05f)   
        {   //we set carholder position to next pos
            carHolder.transform.position = new Vector3(carHolder.position.x, carHolder.position.y, nextPos);
            scrolling = false;  //and set scroling false
        }

    }

    //called when we swipe left
    public void MoveLeft()
    {   //check if scrolling is false and current vehicle index is 1 less than total vehicles
        if (scrolling == false && currentVehicle < itemData.Length - 1)
        {
            currentVehicle++;                       //we increse the index
            scrolling = true;                       //set scrolling true
            nextPos   = carHolder.position.z - 7;   //set the next position
            BtnCode();                              //this set the select btn
        }
    }

    //called when we swipe right
    public void MoveRight()
    {    //check if scrolling is false and current vehicle index is more than 0
        if (scrolling == false && currentVehicle > 0)
        {   
            currentVehicle--;                       //we decrese the index
            scrolling = true;                       //set scrolling true
            nextPos   = carHolder.position.z + 7;   //set the next position
            BtnCode();                              //this set the select btn
        }
    }

    //method to open shop
    public void OpenShop()
    {
        UIManager.instance.ClickSound();            //a click sound

        for (int i = 0; i < GameManager.instance.skinUnlocked.Length; i++)     //loop throungh all the shop item
        {
            itemData[i].unlocked = GameManager.instance.skinUnlocked[i];       //set there unlock value with respective to the store value in GameManager data
        }
        BtnCode();
        coinText.text = "Coins: " + GameManager.instance.coinAmmount;
    }

    void BtnCode()
    {
        vehicleName.text = itemData[currentVehicle].name;                                                     //set the character name

        if (itemData[currentVehicle].unlocked && currentVehicle != GameManager.instance.selectedSkin)         //if character is unlocked and its not selected
        {
            lockObj.SetActive(false);                                                                         //deactivate lock gameobject
            selectBtnText.text = "Select";                                                                    //set the btn text to select
        }

        else if (itemData[currentVehicle].unlocked && currentVehicle == GameManager.instance.selectedSkin)    //if character is unlocked and its  selected
        {
            lockObj.SetActive(false);                                                                         //deactivate lock gameobject
            selectBtnText.text = "Selected";                                                                  //set the btn text to selected
        }

        else if (!itemData[currentVehicle].unlocked)                                                          //if character is locked
        {
            lockObj.SetActive(true);                                                                          //activate lock gameobject
            selectBtnText.text = itemData[currentVehicle].cost + " Coins";                                    //set the btn text to cost
        }
    }

    public void SelectBtn()
    {
        if (itemData[currentVehicle].unlocked == false)                                                  //if the current character is not unlocked
        {
            if (GameManager.instance.coinAmmount >= itemData[currentVehicle].cost)                       //we check if we have enought coins to buy
            {
                GameManager.instance.coinAmmount -= itemData[currentVehicle].cost;                       //we then reduce the total coins by the cost of character
                itemData[currentVehicle].unlocked = true;                                                //unlcok it in itemData
                GameManager.instance.skinUnlocked[currentVehicle] = true;                                //unlcok it in GameManager
                GameManager.instance.selectedSkin = currentVehicle;                                      //set it as selected character
                GameManager.instance.Save();                                                             //Save the data on device

                lockObj.SetActive(false);                                                                //deactivate lock object
                selectBtnText.text = "Selected";                                                         //set select btn text to Selected
                coinText.text = "Coins: " + GameManager.instance.coinAmmount;                            //update conis text
                PlayerCarSpawner.instance.SpawnTheCar();
            }
            else if (GameManager.instance.coinAmmount < itemData[currentVehicle].cost)                   //if we dont have enough money
                UIManager.instance.OpenCoinShop();                                                       //you can open the IAP menu here
        }
        else if (itemData[currentVehicle].unlocked == true)                                              //the character is unlocked
        {
            if (currentVehicle == GameManager.instance.selectedSkin)                                     //if the current charater is equal to selected skin
                return;                                                                                  //do nothing

            else if (currentVehicle != GameManager.instance.selectedSkin)                                //if the current charater is not equal to selected skin
            {
                GameManager.instance.selectedSkin = currentVehicle;                                      //select the character
                GameManager.instance.Save();                                                             //save it
                selectBtnText.text = "Selected";                                                         //change the text
                PlayerCarSpawner.instance.SpawnTheCar();
            }
        }
        UIManager.instance.ClickSound();
    }

}
