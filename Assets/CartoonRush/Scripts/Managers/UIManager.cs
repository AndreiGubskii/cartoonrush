﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public string storeLink;
    public AudioClip btnClick;
    public GameObject mainCamera, garage , world;
    public Image soundImage;
    public Sprite[] soundSprite; // 1 is on and 0 is off
    public Button doubleRewardAdsBtn, normalAdsBtn,noAds;

    public GameObject mainMenuPanel, inGamePanel, unpausedPanel, pausedPanel, gameOverPanel, shopPanel, main1, shop, coinShop;
    public Text distanceText, coinText, gameOverDistanceText, gameOverBestText, gameOverCointext;

    private bool gameStarted = false, shopActive = false;
    [HideInInspector]
    public AudioSource audioS;

    public bool GameStarted { get { return gameStarted; } }
    public bool ShopActive { get { return shopActive; } }

    void Awake()
    {
        if (instance == null) instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        audioS = GetComponent<AudioSource>();
        Time.timeScale = 1;
        gameStarted = false;
        GameManager.instance.gameOver = false;
        GameManager.instance.currentCoinsEarned = 0;
        GameManager.instance.currentDistance = 0;
        distanceText.text = GameManager.instance.currentDistance + "m";
        coinText.text = "" + GameManager.instance.currentCoinsEarned;

        if (GameManager.instance.isMusicOn == true)
        {
            AudioListener.volume = 1;
            soundImage.sprite = soundSprite[1];
        }
        else
        {
            AudioListener.volume = 0;
            soundImage.sprite = soundSprite[0];
        }

        if (GameManager.instance.restart)
        {
            GameManager.instance.restart = false;
            Playbtn();
        }

        if (UnityAds.instance.RewardAdReady == true)
        {
            normalAdsBtn.interactable = true;
        }
        else
        {
            normalAdsBtn.interactable = false;
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GameManager.instance.gameOver == false && gameStarted == true)
        {
            distanceText.text = Mathf.Round(GameManager.instance.currentDistance) + "m";
        }

       /*   включить когда появятся внутриигровые покупки
            if (GameManager.instance.canShowAds && !noAds.gameObject.activeSelf){
                noAds.gameObject.SetActive(true);
            }else if(!GameManager.instance.canShowAds && noAds.gameObject.activeSelf){
                noAds.gameObject.SetActive(false);
            }
        
        */
    }

    public void ClickSound()
    {
        audioS.PlayOneShot(btnClick);
    }

    #region MainMenu

    public void Playbtn()
    {
        ClickSound();
        mainMenuPanel.SetActive(false);
        inGamePanel.SetActive(true);
        gameStarted = true;
        LevelController.instance.StartMoving();
        CameraFollow.instance.SetTarget();
    }

    public void SoundBtn()
    {
        ClickSound();
        if (GameManager.instance.isMusicOn == true)
        {
            GameManager.instance.isMusicOn = false;
            AudioListener.volume = 0;
            soundImage.sprite = soundSprite[0];
            GameManager.instance.Save();
        }
        else
        {
            GameManager.instance.isMusicOn = true;
            AudioListener.volume = 1;
            soundImage.sprite = soundSprite[1];
            GameManager.instance.Save();
        }
    }

    public void ShopBtn()
    {
        ClickSound();
        main1.SetActive(false);
        world.SetActive(false);
        mainCamera.SetActive(false);
        ShopManager.instance.OpenShop();
        shop.SetActive(true);
        garage.SetActive(true);
        shopActive = true;
    }

    public void CloseShopBtn()
    {
        ClickSound();
        main1.SetActive(true);
        world.SetActive(true);
        mainCamera.SetActive(true);
        shop.SetActive(false);
        garage.SetActive(false);
        shopActive = false;
    }

    public void OpenCoinShop()
    {
        ClickSound();
        coinShop.SetActive(true);
    }

    public void CloseCoinShop()
    {
        ShopManager.instance.coinText.text = "Coins: " + GameManager.instance.coinAmmount;
        ClickSound();
        coinShop.SetActive(false);
    }

    public void Purchase1000Coins()
    {
        ClickSound();
       // Purchaser.instance.Buy1000Coins();
    }

    public void Purchase2000Coins()
    {
        ClickSound();
      //  Purchaser.instance.Buy2000Coins();
    }

    public void Purchase4000Coins()
    {
        ClickSound();
       // Purchaser.instance.Buy4000Coins();
    }

    public void LeaderboardBtn()
    {
        ClickSound();

#if GooglePlayDef && UNITY_ANDROID
        GooglePlayManager.singleton.OpenLeaderboardsScore();
#endif

#if UNITY_IOS
        LeaderboardiOSManager.instance.ShowLeaderboard();
#endif
    }

    public void AchievementsBtn()
    {
        ClickSound();

#if GooglePlayDef && UNITY_ANDROID
        GooglePlayManager.singleton.OpenAchievements();
#endif

#if UNITY_IOS
        LeaderboardiOSManager.instance.ShowAchievement();
#endif

    }

    public void NoAdsBtn()
    {
        ClickSound();
      //  Purchaser.instance.BuyNoAds();
    }

    public void RateBtn()
    {
        ClickSound();
        Application.OpenURL(storeLink);
    }

    #endregion

    #region GameMenu

    public void PauseBtn()
    {
        ClickSound();
        Time.timeScale = 0;
        unpausedPanel.SetActive(false);
        pausedPanel.SetActive(true);
    }

    public void ResumeBtn()
    {
        ClickSound();
        Time.timeScale = 1;
        unpausedPanel.SetActive(true);
        pausedPanel.SetActive(false);
    }

    public void RestartBtn()
    {
        ClickSound();
        GameManager.instance.restart = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void HomeBtn()
    {
        ClickSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    #endregion

    #region GameOverMenu

    public void ShareScreenshotBtn()
    {
        ClickSound();
        ShareScreenShot.instance.ShareMethode();
    }

    public void NormalRewardAdsBtn()
    {
        ClickSound();

        #if UNITY_ADS
        UnityAds.instance.ShowRewardedAd(false);
        #endif
    }

    public void DoubleRewardAdsBtn()
    {
        ClickSound();

        #if UNITY_ADS
        UnityAds.instance.ShowRewardedAd(true);
        #endif
    }

    #endregion

    public IEnumerator GameOver()
    {
        GameManager.instance.gameOver = true;

        yield return new WaitForSeconds(1f);

        gameOverDistanceText.text = Mathf.Round(GameManager.instance.currentDistance) + " m";
        gameOverCointext.text     = "Coins: " + GameManager.instance.currentCoinsEarned;

        if (GameManager.instance.currentDistance > GameManager.instance.bestDistance)
            GameManager.instance.bestDistance = (int)GameManager.instance.currentDistance;

        GameManager.instance.lastDistance = (int)GameManager.instance.currentDistance;

        gameOverBestText.text = "best " + GameManager.instance.bestDistance + " m";

        GameManager.instance.Save();

        if (UnityAds.instance.RewardAdReady && !Advertisement.isShowing)
        {
            doubleRewardAdsBtn.gameObject.SetActive(true);
        }
        else
        {
            doubleRewardAdsBtn.gameObject.SetActive(false);
        }

        inGamePanel.SetActive(false);
        gameOverPanel.SetActive(true);


    }

}
