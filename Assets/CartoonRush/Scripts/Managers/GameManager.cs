﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private GameData data;

    public bool isGameStartedFirstTime, canShowAds, gameOver = false, restart;
    public bool allCarsUnlocked;
    public PlayerController currentPlayer;
    public int currentCoinsEarned, lastDistance;
    public float currentDistance;

    [HideInInspector]
    public bool isMusicOn;
    public int totalCar = 7, totalMoney = 1000;
    public bool[] skinUnlocked, achievements;
    public int selectedSkin, gamesPlayed, targetFrameRate = 60;
    public int coinAmmount;  //The ammount of coins the player has
    public int bestDistance = 0;                                  //The best distance the player has reached

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        for (int i = 0; i < skinUnlocked.Length; i++)
        {
            if (skinUnlocked[i] == false) return;

            allCarsUnlocked = true;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
        Application.targetFrameRate = targetFrameRate;
        MakeSingleton();
        InitializeGameVariables();
    }

    void MakeSingleton()
    {
        //this state that if the gameobject to which this script is attached , if it is present in scene then destroy the new one , and if its not present
        //then create new 
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

    }

    void InitializeGameVariables()
    {
        Load();
        if (data != null)
        {
            isGameStartedFirstTime = data.getIsGameStartedFirstTime();
        }
        else
        {
            isGameStartedFirstTime = true;
        }

        if (isGameStartedFirstTime)
        {
            isGameStartedFirstTime = false;
            canShowAds = true;
            isMusicOn = true;
            bestDistance = 0;
            coinAmmount = totalMoney;

            skinUnlocked = new bool[totalCar];
            skinUnlocked[0] = true;
            for (int i = 1; i < skinUnlocked.Length; i++)
            {
                skinUnlocked[i] = false;
            }
            selectedSkin = 0;

            achievements = new bool[5];//if you want ot add more achievements change the value here
            for (int i = 0; i < achievements.Length; i++)
            {
                achievements[i] = false;
            }

            data = new GameData();

            data.setIsGameStartedFirstTime(isGameStartedFirstTime);
            data.setCanShowAds(canShowAds);
            data.setMusicOn(isMusicOn);
            data.setBestDistance(bestDistance);
            data.setSkinUnlocked(skinUnlocked);
            data.setCoins(coinAmmount);
            data.setSelectedSkin(selectedSkin);
            data.setAchievementsUnlocked(achievements);

            Save();
            Load();
        }
        else
        {
            isGameStartedFirstTime = data.getIsGameStartedFirstTime();
            isMusicOn = data.getMusicOn();
            canShowAds = data.getCanShowAds();
            bestDistance = data.getBestDistancee();
            coinAmmount = data.getCoins();
            selectedSkin = data.getSelectedSkin();
            skinUnlocked = data.getSkinUnlocked();
            achievements = data.getAchievementsUnlocked();
        }
    }


    //                              .........this function take care of all saving data like score , current player , current weapon , etc
    public void Save()
    {
        FileStream file = null;
        //whicle working with input and output we use try and catch
        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            file = File.Create(Application.persistentDataPath + "/GameData.dat");

            if (data != null)
            {
                data.setIsGameStartedFirstTime(isGameStartedFirstTime);
                data.setCanShowAds(canShowAds);
                data.setMusicOn(isMusicOn);
                data.setBestDistance(bestDistance);
                data.setSkinUnlocked(skinUnlocked);
                data.setCoins(coinAmmount);
                data.setSelectedSkin(selectedSkin);
                data.setAchievementsUnlocked(achievements);

                bf.Serialize(file, data);
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (file != null)
            {
                file.Close();
            }
        }


    }
    //                            .............here we get data from save
    public void Load()
    {
        FileStream file = null;
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            file = File.Open(Application.persistentDataPath + "/GameData.dat", FileMode.Open);
            data = (GameData)bf.Deserialize(file);

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (file != null)
            {
                file.Close();
            }
        }
    }

    //for resetting the gameManager

    public void ResetGameManager()
    {
        isGameStartedFirstTime = true;
        bestDistance = 0;
        coinAmmount = totalMoney;
        canShowAds = true;
        isMusicOn = true;

        skinUnlocked = new bool[totalCar];
        skinUnlocked[0] = true;
        for (int i = 1; i < skinUnlocked.Length; i++)
        {
            skinUnlocked[i] = false;
        }
        selectedSkin = 0;

        achievements = new bool[5];//if you want ot add more achievements change the value here
        for (int i = 0; i < achievements.Length; i++)
        {
            achievements[i] = false;
        }

        data = new GameData();

        data.setIsGameStartedFirstTime(isGameStartedFirstTime);
        data.setCanShowAds(canShowAds);
        data.setMusicOn(isMusicOn);
        data.setBestDistance(bestDistance);
        data.setSkinUnlocked(skinUnlocked);
        data.setCoins(coinAmmount);
        data.setSelectedSkin(selectedSkin);
        data.setAchievementsUnlocked(achievements);

        Save();
        Load();

        Debug.Log("GameManager Reset");
    }

}

[Serializable]
class GameData
{
    private bool isGameStartedFirstTime, canShowAds;
    private int bestDistance;
    private bool isMusicOn;
    private bool[] skinUnlocked, achievements;
    private int selectedSkin;
    private int coins; //to buy new skins

    public void setIsGameStartedFirstTime(bool isGameStartedFirstTime)
    {
        this.isGameStartedFirstTime = isGameStartedFirstTime;
    }

    public bool getIsGameStartedFirstTime()
    {
        return this.isGameStartedFirstTime;
    }


    //                                                                    ...............music
    public void setMusicOn(bool isMusicOn)
    {
        this.isMusicOn = isMusicOn;

    }

    public bool getMusicOn()
    {
        return this.isMusicOn;

    }
    //                                                                      .......music

    //best score
    public void setBestDistance(int bestDistance)
    {
        this.bestDistance = bestDistance;
    }

    public int getBestDistancee()
    {
        return this.bestDistance;
    }

    //points
    public void setCoins(int coins)
    {
        this.coins = coins;
    }

    public int getCoins()
    {
        return this.coins;
    }

    //skin unlocked
    public void setSkinUnlocked(bool[] skinUnlocked)
    {
        this.skinUnlocked = skinUnlocked;
    }

    public bool[] getSkinUnlocked()
    {
        return this.skinUnlocked;
    }

    //selectedSkin
    public void setSelectedSkin(int selectedSkin)
    {
        this.selectedSkin = selectedSkin;
    }

    public int getSelectedSkin()
    {
        return this.selectedSkin;
    }

    //achievements unlocked
        public void setAchievementsUnlocked(bool[] achievements)
        {
            this.achievements = achievements;
        }

        public bool[] getAchievementsUnlocked()
        {
            return this.achievements;
        }

    //can show ads
    public void setCanShowAds(bool canShowAds)
    {
        this.canShowAds = canShowAds;
    }

    public bool getCanShowAds()
    {
        return this.canShowAds;
    }
    

}