﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateObj : MonoBehaviour {

    public float time;

    public IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
