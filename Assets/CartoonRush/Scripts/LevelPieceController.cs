﻿using UnityEngine;

/// <summary>
/// This script controls the level piece transform
/// </summary>

public class LevelPieceController : MonoBehaviour {

    public bool startMoving = false;    //to make the piece move or stop
    [HideInInspector]
    public PlayerController player;     //ref to the player
	
	// Update is called once per frame
	void Update ()
    {
        //if game is over or game is not started we return from Update method
        if (GameManager.instance.gameOver == true || UIManager.instance.GameStarted == false) return;

        if (startMoving)                //if startmoving is true
            MoveThePiese();             //we call move method
    }

    //this method make the piece move along z axis
    void MoveThePiese()
    {   
        transform.Translate(-Vector3.forward * player.ZMoveSpeed * LevelController.instance.piecesMoveSpeed * Time.deltaTime);
    }

}
