﻿using UnityEngine;

/// <summary>
/// This script controls set of vehicles
/// </summary>

public class SetController : MonoBehaviour {

    public GameObject[] childVehicles;        //ref to vehicles in the set
    public Vector3[]    vehiclesTransform;    //ref to default position of cars

    private GameObject   mainCamera;           //ref to main camera
    private float        moveSpeed;            //speed of set to move                               
    private Vector3      spawnedPos;           // spawn pos
    int                  i = 0;

    // Use this for initialization
    void Start ()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");       //getting the camera in scene
    }

    public void DefaultSettings()                                           //called when set is made active in scene by carSpawner script
    {
        spawnedPos = transform.position;                                    //ref its transfrom
        i = 0;                                                              
        for (int i = 0; i < childVehicles.Length; i++)                      //loop through all the childrens
        {
            childVehicles[i].transform.localPosition = vehiclesTransform[i];        //set there transfrom to default position
            childVehicles[i].GetComponent<EnemyCarController>().DefaultSettings();  //and call the default method of children cars
        }
    }

	// Update is called once per frame
	void Update ()
    {   //if game is over or game is not started we return from Update method
        if (GameManager.instance.gameOver == true || UIManager.instance.GameStarted == false) return;

        if ((transform.position.z - spawnedPos.z) <= -40 && i == 0)         //when the distance between gameobject and player is -40
        {
            i++;
            for (int i = 0; i < childVehicles.Length; i++)                  //we call all the children
            {
                if (childVehicles[i].GetComponent<EnemyCarController>().moveType != MoveType.straight)
                {
                    childVehicles[i].GetComponent<EnemyCarController>().changeLane = true;    //and ask them to change there lane
                }
            }
        }

        moveSpeed = LevelController.instance.piecesMoveSpeed;               //speed assign from levelController

        if ((transform.position.z - mainCamera.transform.position.z) < -6)  //when the distance between gameobject and camera is less than -40
        {
            gameObject.SetActive(false);                                    //we deactivate the gameobject
        }       
                                                                            //else we keep moving
        else transform.Translate(new Vector3(0, 0, -1 * GameManager.instance.currentPlayer.ZMoveSpeed * moveSpeed * Time.deltaTime * 0.75f));
    }
}
