﻿using UnityEngine;

/// <summary>
/// This script follows the vehicle on X axis
/// </summary>

public class CameraFollow : MonoBehaviour {

    public static CameraFollow instance; //me make it an instance so it become easy for us to get its reference in other scripts

    private Transform targetTransform;   //ref to player vehicle which is active in scene
    public float      offset;            //this is to keep distance from the vehicle

    private bool startFollowing = false; //this make the camera to follow the vehicle

    void Awake()
    {
        if (instance == null)   
            instance = this;
    }

    public void SetTarget()                                                 //this method set the target reference
    {
        targetTransform = GameManager.instance.currentPlayer.transform;     //we get the gamemanager reference and store it.

        offset = targetTransform.position.z - transform.position.z;         //we set the z axis offset , so the camera dont move on z axis

        startFollowing = true;                                              //we then set it to follow
    }

	// Update is called once per frame
	void Update ()
    {
	    if(startFollowing == true)                                          //if start following is true we make camera to change its x position
            transform.position = new Vector3(targetTransform.position.x, transform.position.y, targetTransform.position.z - offset);
    }
}
