﻿using System;
using UnityEngine;

/// <summary>
/// This script detect the swipe (left or right)
/// </summary>

public class InputController : MonoBehaviour {

    public static InputController instance;         //me make it an instance so it become easy for us to get its reference in other scripts
    public int toucheCount;                         //to keep track of total touches

    private float tSensitivity = 15;                //how sensitve the swipe must be
    private float swipe_Initial_X, swipe_Final_X;   //initial X and final X position
    private float present_Input_X;                  
    private float swipe_Distance;                   //total distance swiped
    private GameObject cameraObj;
    private bool btnClicked = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        swipe_Initial_X = 0.0f;                     //initially we want it or zero
        swipe_Final_X = 0.0f;
        present_Input_X = 0.0f;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !btnClicked) //when mouse is clicked or finger touch screen and touch count in zero
        {
            if (toucheCount == 0)
            {
                swipe_Initial_X = Input.mousePosition.x;              //we save the x value
                toucheCount = 1;                                      //and increase touch to 1
            }
        }

        if (toucheCount == 1)                                     //if touch is 1 we save x value to final swipe
        {
            swipe_Final_X = Input.mousePosition.x;
        }

        SwipeDirection();                                         //decide the swipe direction

        if (Input.GetKeyUp(KeyCode.Mouse0))                       //when mouse or finger is lifted
        {
            toucheCount = 0;                                      //touche is set to zero
        }
    }

    //this method decide the direction of swipe
    void SwipeDirection()
    {
        if (toucheCount != 1)                                     //if touch is not equal to 1 we return
            return;

        present_Input_X = swipe_Final_X - swipe_Initial_X;        //present X value is defference between final and initial X value

        swipe_Distance = Mathf.Sqrt(Mathf.Pow((swipe_Final_X - swipe_Initial_X), 2)); //we calculate the swipe distance

        if (swipe_Distance <= (Screen.width / tSensitivity))      //check if diatcne is less or equal to require values
            return;                                               //if yes we return

        if (present_Input_X > 0)                                  //if present x is more than 0 , it meanse we swipe to right
        {   //if game is not started and shop panel is active
            if (UIManager.instance.GameStarted == false && UIManager.instance.ShopActive == true)
            {
                ShopManager.instance.MoveRight();                     //we call shop script method
            }
            else if (UIManager.instance.GameStarted == true && Time.timeScale == 1)          //if game is started
            {
                GameManager.instance.currentPlayer.MovingRightSide(); //we call car script method
            }
            
            toucheCount = -1;                                         //make touches -1
        }
        else if (present_Input_X < 0)                                 //if present x is less than 0 , it meanse we swipe to left
        {   //if game is not started and shop panel is active
            if (UIManager.instance.GameStarted == false && UIManager.instance.ShopActive == true)
            {
                ShopManager.instance.MoveLeft();                      //we call shop script method
            }
            else if (UIManager.instance.GameStarted == true && Time.timeScale == 1)          //if game is started
            {
                GameManager.instance.currentPlayer.MovingLeftSide();  //we call car script method
            }
            
            toucheCount = -1;                                         //make touches -1
        }
        else
            toucheCount = 0;                                          //make touches 0
    }

    public void Brake(bool val)                                       //called when brake is pressed
    {
        GameManager.instance.currentPlayer.braking = val;
        btnClicked = val;
    }

    public void Acceleration(bool val)                                     //called when Acceleration is pressed
    {
        GameManager.instance.currentPlayer.acceleration = val;
        btnClicked = val;
    }
}
