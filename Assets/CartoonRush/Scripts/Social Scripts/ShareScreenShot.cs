﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class ShareScreenShot : MonoBehaviour
{
    public static ShareScreenShot instance;
    private bool isProcessing = false;


    public string appUrl = "";

    public string subject = "Your game name";

    string destination;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Update() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Space)) {
            TakeScreenshot();
        }
    }

    private void ShareScreenshotWithText(string text, string subject)
    {
        string screenShotPath = Path.Combine(Application.persistentDataPath, "screenshot" + ".png");
        
        StartCoroutine(delayedShare(screenShotPath, text, subject));
    }
    IEnumerator delayedShare(string screenShotPath, string text, string subject){
        yield return new WaitForEndOfFrame();
        TakeScreenshot();
        while (!File.Exists(screenShotPath)){
            yield return new WaitForEndOfFrame();
        }
        File.WriteAllBytes(screenShotPath, File.ReadAllBytes(screenShotPath));

        NativeShare.Share(text, screenShotPath, "", subject, "image/png", false, "");
    }

    //function called from a button
    public void TakeScreenshot()
    {
        if (!isProcessing){
            StartCoroutine(ShareScreenshot());
        }
    }


    public IEnumerator ShareScreenshot(){
        isProcessing = true;
        // wait for graphics to render
        yield return new WaitForEndOfFrame();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
        // create the texture
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        // put buffer into texture
        screenTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        // apply
        screenTexture.Apply();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
        byte[] dataToSave = screenTexture.EncodeToPNG();
        destination = Path.Combine(Application.persistentDataPath, "screenshot" + ".png");
        Debug.Log(destination);
        File.WriteAllBytes(destination, dataToSave);
        isProcessing = false;
    }

    public void ShareMethode(){
        if (!Application.isEditor) {
            if (Application.systemLanguage == SystemLanguage.Russian) {
                ShareScreenshotWithText(string.Format("Мой результат в игре \"{1}\". Попробуй и ты! {0} ", appUrl, subject), string.Format("Я играю в {0} !", subject));
            }
            else if(Application.systemLanguage == SystemLanguage.Dutch) {
                ShareScreenshotWithText(string.Format("Mein Ergebnis im Spiel \"{1}\". Versuchen Sie und Sie! {0} ", appUrl, subject), string.Format("Ich spiele rein {0} !", subject));
            }
            else if (Application.systemLanguage == SystemLanguage.French){
                ShareScreenshotWithText(string.Format("Mon résultat dans le jeu \"{1}\". Essayez et vous! {0} ", appUrl, subject), string.Format("Je joue dans {0} !", subject));
            }
            else {
                ShareScreenshotWithText(string.Format("My result in the game \"{1}\". Try and you! {0}", appUrl, subject), string.Format("Я играю в {0} !", subject));
            }
            
        }
    }
}