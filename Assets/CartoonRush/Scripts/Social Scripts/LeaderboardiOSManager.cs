﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
using UnityEngine.SceneManagement;

public class LeaderboardiOSManager : MonoBehaviour
{

    public static LeaderboardiOSManager instance;

    private string leaderBoardID;

    //provide your achievement IDs here , depending on number of achievements you can add more or less
    private const string Master = "com.compnayname.demo.achievement1";
    private const string Pro = "com.companyname.demo.achievement2";
    private const string God = "com.companyname.demo.achievement3";
    private const string UnlockMaster = "com.companyname.demo.achievement4";
    private const string UnlockPolice = "com.companyname.demo.achievement4";

    private string[] achievements_names = { Master, Pro, God, UnlockMaster, UnlockPolice };

    private bool[] achievements; //ref variable to check is achievemnt is unlocked or not

#region GAME_CENTER	

    /// <summary>
    /// Authenticates to game center.
    /// </summary>

    [HideInInspector]
    public ManageVariables vars;

    void OnEnable()
    {
        vars = Resources.Load<ManageVariables>("ManageVariablesContainer");
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void Awake()
    {
        MakeInstance();
    }

    void MakeInstance()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        leaderBoardID = vars.leaderBoardID;
        AuthenticateToGameCenter();
    }

    void InitializeAchievements()
    {   //we assign all the values stored in game manager to bool array
        achievements = GameManager.instance.achievements;
        //we call a for loop to check all achievement status
        /*Uncomment
        for (int i = 0; i < achievements.Length; i++)
        {   //is achievement is not unlocked
            if (!achievements[i])
            {   //we call the achievement and set its progress to zero
                Social.ReportProgress(achievements_names[i], 0.0f, (bool success) =>
                {
                    //handle success
                });
            }
        }Uncomment
        */
    }

    public void AuthenticateToGameCenter()
    {
#if UNITY_IOS
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Authentication successful");
                Social.LoadAchievements(ProcessLoadedAchievements); //call to load the achievementts
                InitializeAchievements();
            }
            else
            {
                Debug.Log("Authentication failed");
            }
        });
#endif
    }

    /// <summary>
    /// Reports the score on leaderboard.
    /// </summary>
    /// <param name="score">Score.</param>
    /// <param name="leaderboardID">Leaderboard I.</param>

    public static void ReportScore(long score, string leaderboardID)
    {

#if UNITY_EDITOR

        Debug.Log("Working");

#elif UNITY_IOS
    //Debug.Log("Reporting score " + score + " on leaderboard " + leaderboardID);
    Social.ReportScore(score, leaderboardID, success =>
		{
		if (success)
		{
			Debug.Log("Reported score successfully");
		}
		else
		{
			Debug.Log("Failed to report score");
		}

		Debug.Log(success ? "Reported score successfully" : "Failed to report score"); Debug.Log("New Score:"+score);  
	});
#endif
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
#if UNITY_IOS
	if (Social.localUser.authenticated == true)
    {
        CheckIfAnyUnlockedAchievements();
        ReportScore(Mathf.RoundToInt(GameManager.instance.lastDistance), leaderBoardID);
    }
#endif
    }

    /// <summary>
    /// Shows the leaderboard UI.
    /// </summary>

    public void ShowLeaderboard()
    {
#if UNITY_IOS
        Social.ShowLeaderboardUI();
#endif
    }

    // <summary>
    /// Shows the achievement UI.
    /// </summary>
    public void ShowAchievement()
    {
#if UNITY_IOS
        Social.ShowAchievementsUI();
#endif
    }
    //this is to check if we have any achievements in game center
    void ProcessLoadedAchievements(IAchievement[] achievements)
    {
        if (achievements.Length == 0)
            Debug.Log("Error: no achievements found");
        else
            Debug.Log("Got " + achievements.Length + " achievements");
    }

    //this method is called when scene is reloaded or changed to check for achievements . you can also call it in update but then it will be called every frame
    void CheckIfAnyUnlockedAchievements()
    {
        if (GameManager.instance != null)                                           //Achievemtn 1
        {
            if (GameManager.instance.lastDistance >= 300)
            {
                if (!achievements[0])
                    UnlockAchievements(0);
            }

            if (GameManager.instance.lastDistance >= 600)                           //Achievemtn 2
            {
                if (!achievements[1])
                    UnlockAchievements(1);
            }

            if (GameManager.instance.lastDistance >= 1200)                          //Achievemtn 3
            {
                if (!achievements[2])
                    UnlockAchievements(2);
            }

            //we check if all the cars are unlocked
            if (GameManager.instance.allCarsUnlocked == true)                       //Achievemtn 4
            {
                if (!achievements[3])
                    UnlockAchievements(3);
            }

            //we check if the 4th element of bool array is true (it represent the police car )
            if (GameManager.instance.skinUnlocked[3] == true)                       //Achievemtn 5
            {
                if (!achievements[4])
                    UnlockAchievements(4);
            }
        }
    }

    void UnlockAchievements(int index)
    {
        if (Social.localUser.authenticated)
        {
            /*                                                                                                      Uncomment
            Social.ReportProgress(achievements_names[index], 100.0f, (bool success) =>
            {
                if (success)
                {
                    //sound.Play(); //here we play the soucd when we achieve new achievement
                    achievements[index] = true;
                    GameManager.instance.achievements = achievements;
                    GameManager.instance.Save();
                }
            });                                                                                                      Uncomment
            */
        }
    }



#endregion
}