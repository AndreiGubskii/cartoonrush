﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

public class UnityAds : MonoBehaviour
{

    public static UnityAds instance;

    private int i = 0;
    private bool rewardAdReady = false;

    private bool doubleCoins = false;

    public bool RewardAdReady
    {
        get { return rewardAdReady; }
    }

    [HideInInspector]
    public ManageVariables vars;

    [SerializeField] public string gameID;
    [SerializeField] public bool testMode;
    void OnEnable()
    {
        
        vars = Resources.Load<ManageVariables>("ManageVariablesContainer");
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
#if UNITY_ADS
        Advertisement.Initialize(gameID, testMode);
#endif
    }

    // Use this for initialization
    void Start()
    {
        i = 0;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
#if UNITY_ADS
        if (Advertisement.IsReady("rewardedVideo"))
        {
            rewardAdReady = true;
        }
        else if (!Advertisement.IsReady("rewardedVideo"))
        {
            rewardAdReady = false;
        }
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance == null)
            return;

        if (GameManager.instance.gameOver == true)
        {
            //we want only one ad to be shown so we put condition that when i is 0 we show ad.
            if (i == 0)
            {
                i++;
                GameManager.instance.gamesPlayed++;

                if (GameManager.instance.gamesPlayed >= vars.showInterstitialAfter)
                {
                    GameManager.instance.gamesPlayed = 0;
                    //use any one of them
                    //admob ads
                    //AdsManager.instance.ShowInterstitial();
                    //unity ads
                    ShowAd();
                }
            }
        }
    }

    public void ShowAd()
    {
#if UNITY_ADS
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
#endif
    }

    //use this function for showing reward ads
    public void ShowRewardedAd(bool doubleCoins)
    {
#if UNITY_ADS
        if (Advertisement.IsReady("rewardedVideo"))
        {
            this.doubleCoins = doubleCoins;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        else
        {
            Debug.Log("Ads not ready");
        }
#endif
    }

#if UNITY_ADS
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                if (doubleCoins == true) {
                    doubleCoins = false;
                    if (GameManager.instance.currentCoinsEarned > 5) {
                        GameManager.instance.currentCoinsEarned += GameManager.instance.currentCoinsEarned;
                    }
                    else {
                        GameManager.instance.currentCoinsEarned = 10;
                    }
                    
                    UIManager.instance.doubleRewardAdsBtn.gameObject.SetActive(false);
                }
                else {
                
                    GameManager.instance.coinAmmount += 50; /*here we give 50 poinst as reward*/
                    UIManager.instance.CloseCoinShop();
                    
                }

                GameManager.instance.Save();

                UIManager.instance.coinText.text = "" + GameManager.instance.currentCoinsEarned;
                UIManager.instance.gameOverCointext.text = "" + GameManager.instance.currentCoinsEarned;
                
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");

                break;
        }
    }
#endif

}
